package com.vnfhome.Threads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dock {
    private long product = 0;
    private int ship = 0;
    private int train = 0;
    private int coal = 0;
    private List<Integer> unloadTrainsCap = new ArrayList<>(Arrays.asList(0, 0));

    public synchronized void toDock(int cap, int number) throws Exception {
        int currentTrain = -1;

        while (true) {
            if (train == 0) {
                currentTrain = 0;
                break;
            } else if (train == 1) {
                currentTrain = 1;
                break;
            }
            wait();
        }
        train++;
        notifyAll();
        System.out.println("Поезд №" + number + " встал на разгрузку");
        unloadTrainsCap.set(currentTrain, cap);
        while (ship == 0) {
            wait();
        }
        while (unloadTrainsCap.get(currentTrain) > 0) {
            wait();
        }
        System.out.println("Поезд №" + number + " покинул порт");
        train--;
        notifyAll();
    }

    public synchronized void fromDock(int cap, int number) throws Exception {
        while (train == 0) {
            wait();
        }

        while (ship == 1) {
            wait();
        }
        ship++;
        notifyAll();
        System.out.println("Корабль №" + number + " встал на погрузку");
        int currentCapacity = 0;
        int curUnloadTrain;
        while (currentCapacity != cap) {
            curUnloadTrain = -1;
            while (true) {
                if (unloadTrainsCap.get(0) > 0) {
                    curUnloadTrain = 0;
                    break;
                } else if (unloadTrainsCap.get(1) > 0) {
                    curUnloadTrain = 1;
                    break;
                }
                wait();
            }
            if ((cap - currentCapacity) >= unloadTrainsCap.get(curUnloadTrain)) {
                currentCapacity += unloadTrainsCap.get(curUnloadTrain);
                unloadTrainsCap.set(curUnloadTrain, 0);
            } else {
                unloadTrainsCap.set(curUnloadTrain, unloadTrainsCap.get(curUnloadTrain) - (cap - currentCapacity));
                currentCapacity = cap;
            }
            notifyAll();
        }
        System.out.println("Корабль №" + number + " покинул порт");
        ship--;
        notifyAll();
    }
}
