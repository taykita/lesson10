package com.vnfhome.Threads;

public class Main {

    public static void main(String[] args) {
        Dock dock = new Dock();
        Thread train1 = new Train(dock, 1, 1500, 24);
        Thread train2 = new Train(dock, 2, 1800, 36);
        Thread train3 = new Train(dock, 3, 1200, 27);
        Thread train4 = new Train(dock, 4, 3400, 11);
        Thread train5 = new Train(dock, 5, 2400, 70);
        Thread ship1 = new Ship(dock, 1, 10000, 5*24);
        Thread ship2 = new Ship(dock, 2, 16000, 30*24);
        Thread ship3 = new Ship(dock, 3, 40000, 14*24);
        train1.start();
        train2.start();
        train3.start();
        train4.start();
        train5.start();
        ship1.start();
        ship2.start();
        ship3.start();
    }
}
