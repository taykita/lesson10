package com.vnfhome.Threads;

public class Train extends Thread {
    Dock dock;
    int number;
    int cap;
    int speed;

    Train(Dock dock, int number, int cap, int speed) {
        this.dock = dock;
        this.number = number;
        this.cap = cap;
        this.speed = speed;
    }

    public void run() {
        try {
            while (true) {
                dock.toDock(cap, number);
                sleep(speed * 200L);
            }
        } catch (Exception e) {
        }
    }
}
