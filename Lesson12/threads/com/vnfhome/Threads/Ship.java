package com.vnfhome.Threads;

public class Ship extends Thread {
    Dock dock;
    int number;
    int cap;
    int speed;

    Ship(Dock dock, int number, int cap, int speed) {
        this.dock = dock;
        this.number = number;
        this.cap = cap;
        this.speed = speed;
    }

    public void run() {
        try {
            while (true) {
                dock.fromDock(cap, number);
                sleep(speed * 200L);
            }
        } catch (Exception e) {
        }
    }
}
