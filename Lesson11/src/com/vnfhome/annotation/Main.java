package com.vnfhome.annotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {


        String[] stringArray = {"6", "6"};
        SimpleData simpleData = new SimpleData(7, (byte) 0x07,
                new WithArrayData(8, (byte) 0x08, "8", "8", stringArray), "7");
        SimpleData newSimpleData = new SimpleData(1000, (byte) 0x07,
                new WithArrayData(8, (byte) 0x08, "8", "8", stringArray), "7");
        copy(simpleData, newSimpleData);
        printFieldsValues(newSimpleData);

        /*
        String[] stringArray = {"6", "6"};
        WithArrayData withArrayData = new WithArrayData(6, (byte) 0x06, "6", "6", stringArray);
        WithArrayData newWithArrayData = new WithArrayData();
        copy(withArrayData, newWithArrayData);
        printFieldsValues(newWithArrayData);
        System.out.println(newWithArrayData.getStringArray()[0]);
        */

    }


    private static void printLine() {
        System.out.println("--------------------------------------------");
    }

    private static void printFieldsValues(Object object) {
        printLine();
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                Object value = f.get(object);
                System.out.println(f.getName() + " as " + f.getType().getCanonicalName() + " = " + value);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
    public static void setFromAnnotation(Object obj, Field f, DoNotCopy a) {
        try {
            switch (f.getType().getTypeName()) {
                case "int":
                    f.set(obj, Integer.parseInt(a.arg()));
                    break;
                case "byte":
                    f.set(obj, Byte.parseByte(a.arg()));
                    break;
                case "long":
                    f.set(obj, Long.parseLong(a.arg()));
                    break;
                case "float":
                    f.set(obj, Float.parseFloat(a.arg()));
                    break;
                case "boolean":
                    f.set(obj, Boolean.parseBoolean(a.arg()));
                    break;
                case "java.lang.String":
                    f.set(obj, a.arg());
                    break;
                case "java.lang.String[]":
                    String[] str = a.arg().split(", ");
                    f.set(obj, str);
                    break;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static void copy(Object source, Object dist) {
        Class<?> sourceClass = source.getClass();
        Class<?> distClass = dist.getClass();
        Field[] sourceFields;
        try {
            while (true) {
                sourceFields = sourceClass.getDeclaredFields();
                for (Field sourceField : sourceFields) {
                    Field distField = distClass.getDeclaredField(sourceField.getName());
                    sourceField.setAccessible(true);
                    distField.setAccessible(true);
                    DoNotCopy a = sourceField.getAnnotation(DoNotCopy.class);
                    if (a != null) {
                        if (!a.arg().equals("")) {
                            setFromAnnotation(dist, sourceField, a);
                        }
                    } else {
                        if (sourceField.getType().isArray()) {
                            if (sourceField.get(source) instanceof String[]) {
                                String[] strArFld = new String[((String[]) sourceField.get(source)).length];
                                for (int i = 0; i < ((String[]) sourceField.get(source)).length; i++) {
                                    strArFld[i] = ((String[]) sourceField.get(source))[i];
                                }
                                distField.set(dist, strArFld);
                            }
                        } else if (sourceField.get(source) instanceof Collection) {
                            if (sourceField.get(source) instanceof List) {
                                Class[] params = {Collection.class};
                                List newList = (List) sourceField.get(source).getClass().getConstructor(params).newInstance(sourceField.get(source));
                                distField.set(dist, newList);
                            }
                        } else if (sourceField.get(source) instanceof Map) {
                            Class[] params = {Map.class};
                            Map newMap = (Map) sourceField.get(source).getClass().getConstructor(params).newInstance(sourceField.get(source));
                            distField.set(dist, newMap);
                        } else if (sourceField.get(source) instanceof String) {
                            String str = new String((String) sourceField.get(source));
                            distField.set(dist, str);
                        } else if (!(sourceField.getType().isPrimitive())) {
                            distField.set(dist, copyOf(sourceField.get(source)));
                            copy(sourceField.get(source), distField.get(dist));
                        } else {
                            distField.set(dist, sourceField.get(source));
                        }
                    }

                }
                sourceClass = sourceClass.getSuperclass();
                distClass = distClass.getSuperclass();
            }
        } catch (NullPointerException e) {

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static Object copyOf(Object source) {
        try {
            Constructor<?> constructor = source.getClass().getConstructor();
            Object dist = constructor.newInstance();
            copy(source, dist);
            return dist;
        } catch (Exception e) {
            return null;
        }
    }

}
