package Task2;

import java.util.List;

public class Worker extends Thread {
    Service service;

    public Worker(Service service) {
        this.service = service;
    }

    public void run() {
        try {
            while (true) {
                List<Long> jobs = service.getJobs();
                sleep(50);
            }
        } catch (Exception e) {

        }
    }

}
