package Task2;

public class Main {

    public static void main(String[] args) {
        FakeDB db = new FakeDB();
        final int PRODUCER_NUMBER = 5;
        final int WORKER_NUMBER = 15;
        Thread [] producers = new Thread[PRODUCER_NUMBER];
        Thread [] workers = new Thread[WORKER_NUMBER];
        for(int i = 0; i < producers.length; ++i)
            producers [i] = new Producer(db);
        for(int i = 0; i < workers.length; ++i)
            workers [i] = new Worker(new Service(db));
        for (Thread producer : producers)
            producer.start();
        for (Thread worker : workers)
            worker.start();
    }
}
