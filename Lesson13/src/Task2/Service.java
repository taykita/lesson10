package Task2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Service {
    FakeDB db;

    public Service(FakeDB db) {
        this.db = db;
    }

    public List<Long> getJobs() {
        if (db.next <= db.jobs.size() && db.jobs.get(db.next) < new Date().getTime()) {
            return db.getJobs();
        }
        return new ArrayList<>();
    }
}
