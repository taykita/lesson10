package Task2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FakeDB {
    List<Long> jobs = new ArrayList<>();
    int next;

    public synchronized List<Long> getJobs() {
        List<Long> result = new ArrayList<>();
        int count = 3;
        while (count-- > 0 && next <= jobs.size() && jobs.get(next) < new Date().getTime()) {
            result.add(jobs.get(next++));
        }
        if (result.size() == 0)
            System.out.println("Empty result!");
        else
            System.out.println("Got " + result.size() + "job(s)");
        return result;
    }

    public synchronized void addJobs() {
        jobs.add(new Date().getTime() + 1000);
    }
}
