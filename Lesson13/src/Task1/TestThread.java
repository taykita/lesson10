package Task1;

import java.util.concurrent.Callable;

import static java.lang.Thread.sleep;

public class TestThread implements Callable<Long> {
    // Максимальное время работы потока
    private final int MAX_SLEEP_TIME = 10000;

    @Override
    public Long call() {
        try {
            sleep((long) (Math.random() * MAX_SLEEP_TIME));
            return Math.round(Math.random());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0L;
    }
}
