package Task1;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Task1 {
    public static void main(String[] args) {
        TestThread tread = new TestThread();


        try {
            Future<Long> tread1 = Executors.newCachedThreadPool().submit(tread);
            Future<Long> tread2 = Executors.newCachedThreadPool().submit(tread);
            Future<Long> tread3 = Executors.newCachedThreadPool().submit(tread);
            Future<Long> tread4 = Executors.newCachedThreadPool().submit(tread);
            Future<Long> tread5 = Executors.newCachedThreadPool().submit(tread);

            long answer = tread1.get();
            answer += tread2.get();
            answer += tread3.get();
            if (answer == 3) {
//                System.out.println("1 - SUCCESS");
                System.out.println("SUCCESS");
                System.exit(0);
            }
            answer += tread4.get();
            if (answer == 3) {
//                System.out.println("2 - SUCCESS");
                System.out.println("SUCCESS");
                System.exit(0);
            }
            answer += tread5.get();
            if (answer == 3) {
//                System.out.println("3 - SUCCESS");
                System.out.println("SUCCESS");
                System.exit(0);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("FAILED");
    }
}
