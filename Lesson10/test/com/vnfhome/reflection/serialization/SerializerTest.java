package com.vnfhome.reflection.serialization;

import com.vnfhome.reflection.SimpleData;
import com.vnfhome.reflection.WithArrayData;
import com.vnfhome.reflection.WithCollectionData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SerializerTest {
    String serialObject;
    WithCollectionData testClass;
    @BeforeEach
    public void setStr() {
        serialObject = "com.vnfhome.reflection.WithCollectionData = \n" +
                "{\n" +
                "    listString:java.util.List = \n" +
                "    Окунь,\n" +
                "    Щука,\n" +
                "    Лещь,\n" +
                "    mapStringSimpleData:java.util.Map = \n" +
                "    {\n" +
                "        a,\n" +
                "        {\n" +
                "            intData:int = 0;\n" +
                "            byteData:byte = 0;\n" +
                "        };\n" +
                "    },\n" +
                "    {\n" +
                "        b,\n" +
                "        {\n" +
                "            intData:int = 7;\n" +
                "            byteData:byte = 7;\n" +
                "            objectData:com.vnfhome.reflection.WithArrayData = \n" +
                "            {\n" +
                "                stringArray:java.lang.String[] = \n" +
                "                Береза,\n" +
                "                Ель,\n" +
                "                Дуб,\n" +
                "                intData:int = 8;\n" +
                "                byteData:byte = 8;\n" +
                "                objectData:java.lang.Object = 8;\n" +
                "                stringData:java.lang.String = 8;\n" +
                "            };\n" +
                "            stringData:java.lang.String = 7;\n" +
                "        };\n" +
                "    };\n" +
                "    stringArray:java.lang.String[] = \n" +
                "    Береза,\n" +
                "    Ель,\n" +
                "    Дуб,\n" +
                "    intData:int = 6;\n" +
                "    byteData:byte = 6;\n" +
                "    objectData:java.lang.Object = Кирпич;\n" +
                "    stringData:java.lang.String = Догони меня!;\n" +
                "};";
    }

    @BeforeEach
    public void initTestClass() {
        String[] stringArray = {"Береза", "Ель", "Дуб"};
        SimpleData simpleData = new SimpleData(7, (byte) 0x07, new WithArrayData(8, (byte) 0x08, "8", "8", stringArray), "7");
        List<String> listString = new LinkedList<>();
        listString.add("Окунь");
        listString.add("Щука");
        listString.add("Лещь");
        Map<String, SimpleData> mapStringSimpleData = new TreeMap<>();
        mapStringSimpleData.put("a", new SimpleData());
        mapStringSimpleData.put("b", simpleData);
        testClass = new WithCollectionData(6, (byte) 0x06, "Кирпич", "Догони меня!", stringArray, listString, mapStringSimpleData);
    }

    @Test
    public void serialization() {
        Serializer serializer = new Serializer();
        String actSerialObject = serializer.getClassDesc(testClass);

        assertEquals(serialObject, actSerialObject);
    }

}
