package com.vnfhome.reflection;

import java.util.List;
import java.util.Map;

public class TestCopyClass {
    public TestCopyClass() {
    }

    public TestCopyClass(Object obj) {
        this.obj = obj;
    }

    public TestCopyClass(List<String> listString, Map<Integer, String> mapStringSimpleData) {
        this.listString = listString;
        this.mapIntStrData = mapStringSimpleData;
    }

    public TestCopyClass(String[] str) {
        this.str = str;
    }

    private Object obj;
    private String[] str;
    private List<String> listString;
    private Map<Integer, String> mapIntStrData;

    public Object getObj() {
        return obj;
    }

    public List<String> getListString() {
        return listString;
    }

    public Map<Integer, String> getMapIntStrData() {
        return mapIntStrData;
    }

    public String[] getStr() {
        return str;
    }
}
