package com.vnfhome.reflection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {
    TestClass testObj = new TestClass(5);
    List<String> list;
    Map<Integer, String> map;
    String[] strArr;

    @BeforeEach
    public void beforeTest() {
        strArr = new String[2];
        strArr[0] = "Java";
        strArr[1] = "C++";
        List<String> list = new ArrayList<String>(Arrays.asList("1", "2", "3"));
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "1");
        map.put(2, "2");
        map.put(3, "3");
    }

    @Test
    public void getFieldsNames() {
        String actStr = Main.getFieldsNames(testObj);

        assertEquals("i as int\n", actStr);
    }

    public String prepareActStr(String actStr) {
        for (int i = 0; i < actStr.length(); i++) {
            if (actStr.charAt(i) == '\n') {
                actStr = actStr.substring(0, i + 1);
            }
        }
        return actStr;
    }

    @Test
    public void getMethodsNames() {
        String actStr = Main.getMethodsNames(testObj);
        actStr = prepareActStr(actStr);

        assertEquals("setI return void\n", actStr);
    }


    @Test
    public void copy_Object() {
        TestCopyClass testSourceCopyObj = new TestCopyClass(new SimpleData(6, (byte) 0x06, "6", "6"));
        TestCopyClass testDistCopyObj = new TestCopyClass(new SimpleData());
        Main.copy(testSourceCopyObj, testDistCopyObj);

        assertEquals(((SimpleData) testSourceCopyObj.getObj()).getIntData(), ((SimpleData) testDistCopyObj.getObj()).getIntData());
    }

    @Test
    public void copy_String() {
        TestCopyClass testSourceCopyObj = new TestCopyClass(strArr);
        TestCopyClass testDistCopyObj = new TestCopyClass();
        Main.copy(testSourceCopyObj, testDistCopyObj);

        assertArrayEquals(testSourceCopyObj.getStr(), testDistCopyObj.getStr());
    }

    @Test
    public void copy_ColMap() {
        TestCopyClass testSourceCopyObj = new TestCopyClass(list, map);
        TestCopyClass testDistCopyObj = new TestCopyClass();
        Main.copy(testSourceCopyObj, testDistCopyObj);

        assertEquals(testSourceCopyObj.getListString(), testDistCopyObj.getListString());
        assertEquals(testSourceCopyObj.getMapIntStrData(), testDistCopyObj.getMapIntStrData());
    }

}
