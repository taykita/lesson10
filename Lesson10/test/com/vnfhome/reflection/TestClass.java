package com.vnfhome.reflection;

public class TestClass {
    public TestClass(int i) {
        this.i = i;
    }

    private int i;

    public void setI(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}
