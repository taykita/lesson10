package com.vnfhome.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        SimpleData simpleData = new SimpleData();
        WithCollectionData withCollectionData = new WithCollectionData();
        //1
        printLine();
        System.out.println(getFieldsNames(simpleData));
        printLine();
        System.out.println(getFieldsNames(withCollectionData));
        //2
        printLine();
        System.out.println(getMethodsNames(simpleData));
        printLine();
        System.out.println(getMethodsNames(withCollectionData));
        //3

        String[] stringArray = {"6", "6"};
        simpleData = new SimpleData(7, (byte) 0x07, new WithArrayData(8, (byte) 0x08, "8", "8", stringArray), "7");
        List<String> listString = new LinkedList<>();
        listString.add("1");
        listString.add("2");
        listString.add("3");
        Map<String, SimpleData> mapStringSimpleData = new TreeMap<>();
        mapStringSimpleData.put("a", new SimpleData());
        mapStringSimpleData.put("b", simpleData);
        WithArrayData withArrayData = new WithArrayData(6, (byte) 0x06, "6", "6", stringArray);
        withCollectionData = new WithCollectionData(6, (byte) 0x06, "6", "6", stringArray, listString, mapStringSimpleData);

        //3
        //printFieldsValues(simpleData);
        //printFieldsValues(withCollectionData);
        //4
        //callSetterWhith8(simpleData);
        //printFieldsValues(simpleData);

        //5
        //printFieldsValues(simpleData);
        //printFieldsValues(newSimpleData);

        SimpleData newSimpleData = new SimpleData();
        copy(simpleData, newSimpleData);
        printFieldsValues(newSimpleData);
        WithArrayData sd = (WithArrayData) newSimpleData.getObjectData();
        System.out.println(sd.getIntData());
        System.out.println(sd.getStringArray()[0]);
        System.out.println(simpleData.getObjectData().toString());
        System.out.println(newSimpleData.getObjectData().toString());


        WithArrayData newWithArrayData = new WithArrayData();
        copy(withArrayData, newWithArrayData);
        printFieldsValues(newWithArrayData);
        System.out.println(newWithArrayData.getStringArray()[0]);
        System.out.println(withArrayData.getStringArray().toString());
        System.out.println(newWithArrayData.getStringArray().toString());

        WithCollectionData newWithCollectionData = new WithCollectionData();
        copy(withCollectionData, newWithCollectionData);
        printFieldsValues(newWithCollectionData);

        System.out.println(withCollectionData.getListString().toString());
        newWithCollectionData.getListString().add("4");
        System.out.println(newWithCollectionData.getListString().toString());

        System.out.println(withCollectionData.getMapStringSimpleData().toString());
        System.out.println(newWithCollectionData.getMapStringSimpleData().toString());

        //6
        //printFieldsValues(copyOf(newSimpleData));

        //7
        //benchmark();

    }

    private static void printLine() {
        System.out.println("--------------------------------------------");
    }

    private static String getAllFields(Class cls) {
        Field[] fields;
        StringBuilder strFields = new StringBuilder();
        while (true) {
            try {
                fields = cls.getDeclaredFields();
                for (Field f : fields) {
                    strFields.append(f.getName()).append(" as ").append(f.getType().getCanonicalName()).append("\n");
                }
                cls = cls.getSuperclass();
            } catch (NullPointerException e) {
                return strFields.toString();
            }
        }
    }

    public static String getFieldsNames(Object object) {
        Class cls = object.getClass();
        return getAllFields(cls);
    }

    private static String getAllMethods(Class cls) {
        Method[] methods;
        StringBuilder strMethods = new StringBuilder();
        while (true) {
            try {
                methods = cls.getDeclaredMethods();
                for (Method m : methods) {
                    strMethods.append(m.getName()).append(" return ").append(m.getReturnType().getCanonicalName()).append("\n");
                }
                cls = cls.getSuperclass();
            } catch (NullPointerException e) {
                return strMethods.toString();
            }
        }
    }

    public static String getMethodsNames(Object object) {
        Class cls = object.getClass();
        return getAllMethods(cls);
    }

    private static void printFieldsValues(Object object) {
        printLine();
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                Object value = f.get(object);
                System.out.println(f.getName() + " as " + f.getType().getCanonicalName() + " = " + value);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    private static void callSetterWhith8(Object object) {
        printLine();
        Method[] methods = object.getClass().getDeclaredMethods();
        for (Method m : methods) {
            String name = m.getName();
            if (!name.substring(0, 3).equals("set"))
                continue;
            Class<?>[] argTypes = m.getParameterTypes();
            try {
                switch (argTypes[0].getTypeName()) {
                    case "int":
                        m.invoke(object, 8);
                        break;
                    case "byte":
                        m.invoke(object, (byte) 8);
                        break;
                    default:
                        m.invoke(object, "8");
                        break;
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }


    public static void copy(Object source, Object dist) {
        Class<?> sourceClass = source.getClass();
        Class<?> distClass = dist.getClass();
        Field[] sourceFields;
        try {
            while (true) {
                sourceFields = sourceClass.getDeclaredFields();
                for (Field sourceField : sourceFields) {
                    Field distField = distClass.getDeclaredField(sourceField.getName());
                    sourceField.setAccessible(true);
                    distField.setAccessible(true);

                    if (sourceField.getType().isArray()) {
                        if (sourceField.get(source) instanceof String[]) {
                            //Как копировать более универсально?
                            String[] strArFld = new String[((String[]) sourceField.get(source)).length];
                            for (int i = 0; i < ((String[]) sourceField.get(source)).length; i++) {
                                strArFld[i] = ((String[]) sourceField.get(source))[i];
                            }
                            distField.set(dist, strArFld);
                        }
                    } else if (sourceField.get(source) instanceof Collection) {
                        if (sourceField.get(source) instanceof List) {
                            Class[] params = {Collection.class};
                            List newList = (List) sourceField.get(source).getClass().getConstructor(params).newInstance(sourceField.get(source));
                            distField.set(dist, newList);
                        }
                    } else if (sourceField.get(source) instanceof Map) {
                        Class[] params = {Map.class};
                        Map newMap = (Map) sourceField.get(source).getClass().getConstructor(params).newInstance(sourceField.get(source));
                        distField.set(dist, newMap);
                    } else if (sourceField.get(source) instanceof String) {
                        String str = new String((String) sourceField.get(source));
                        distField.set(dist, str);
                    } else if (!(sourceField.getType().isPrimitive())) {
                        distField.set(dist, copyOf(sourceField.get(source)));
                        copy(sourceField.get(source), distField.get(dist));
                    } else {
                        distField.set(dist, sourceField.get(source));
                    }
                }
                sourceClass = sourceClass.getSuperclass();
                distClass = distClass.getSuperclass();
            }
        } catch (NullPointerException e) {

        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public static Object copyOf(Object source) {
        try {
            Constructor<?> constructor = source.getClass().getConstructor();
            Object dist = constructor.newInstance();
            copy(source, dist);
            return dist;
        } catch (Exception e) {
            return null;
        }
    }

    public static void benchmark() {
        try {
            Date start = new Date();
            SimpleData simpleData = new SimpleData();
            for (int i = 0; i < 1000000000; ++i)
                simpleData.setIntData(500);
            System.out.println(new Date().getTime() - start.getTime());

            Method method = SimpleData.class.getDeclaredMethod("setIntData", Integer.TYPE);
            start = new Date();
            for (int i = 0; i < 1000000000; ++i) {
                method.invoke(simpleData, 500);
            }
            System.out.println(new Date().getTime() - start.getTime());

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
