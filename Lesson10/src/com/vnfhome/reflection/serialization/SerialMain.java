package com.vnfhome.reflection.serialization;

import com.vnfhome.reflection.SimpleData;
import com.vnfhome.reflection.WithArrayData;
import com.vnfhome.reflection.WithCollectionData;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SerialMain {
    public static void main(String[] args) {
        Serializer serializer = new Serializer();
        String[] stringArray = {"Береза", "Ель", "Дуб"};
        SimpleData simpleData = new SimpleData(7, (byte) 0x07, new WithArrayData(8, (byte) 0x08, "8", "8", stringArray), "7");
        List<String> listString = new LinkedList<>();
        listString.add("Окунь");
        listString.add("Щука");
        listString.add("Лещь");
        Map<String, SimpleData> mapStringSimpleData = new TreeMap<>();
        mapStringSimpleData.put("a", new SimpleData());
        mapStringSimpleData.put("b", simpleData);
        WithArrayData withArrayData = new WithArrayData(6, (byte) 0x06, "6", "6", stringArray);
//        System.out.println(serializer.getClassDesc(withArrayData));
        WithCollectionData withCollectionData = new WithCollectionData(6, (byte) 0x06, "Кирпич", "Догони меня!", stringArray, listString, mapStringSimpleData);
        System.out.println(serializer.getClassDesc(withCollectionData));


//        TestClass testClass = new TestClass(6, (byte) 0x06, "6", "6", stringArray, true, 'J');
//        System.out.println(serializer.getClassDesc(testClass));
    }
}
