package com.vnfhome.reflection.serialization;

import com.vnfhome.reflection.SimpleData;

public class TestClass extends SimpleData {
    private String[] stringArray;
    private boolean boolData;
    private char charData;

    public TestClass() {
    }

    public TestClass(int intData, byte byteData, Object objectData, String stringData, String[] stringArray, boolean boolData, char charData) {
        super(intData, byteData, objectData, stringData);
        this.stringArray = stringArray;
        this.boolData = boolData;
        this.charData = charData;
    }


    public String[] getStringArray() {
        return stringArray;
    }

    public void setStringArray(String[] stringArray) {
        this.stringArray = stringArray;
    }
}
