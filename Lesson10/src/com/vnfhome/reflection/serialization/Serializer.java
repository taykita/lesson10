package com.vnfhome.reflection.serialization;

import com.vnfhome.reflection.Main;
import com.vnfhome.reflection.SimpleData;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Serializer {
    public Serializer() {
        this.classDesc = new StringBuilder();
    }

    private StringBuilder classDesc;

    private String checkLocalGetFields(Object obj, int tab) {
        String strFields;
        if (obj instanceof Class) {

        }
        return "";
    }

    private String getTrueFieldDesc(Object obj, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);
        if (obj instanceof String) {
            strFields.append(tabS).append(obj.toString()).append(",\n");
        } else if (obj.getClass().isArray()) {
            tab++;
            strFields.append(getArr((Object[]) obj, tab));
            tab--;
        } else if (obj instanceof Collection) {
            tab++;
            strFields.append(getColl((List) obj, tab));
            tab--;
        } else if (obj instanceof Map) {
            tab++;
            strFields.append(getMap((Map) obj, tab));
            tab--;
        } else {
            switch (obj.getClass().getTypeName()) {
                case "int":
                case "byte":
                case "long":
                case "float":
                case "boolean":
                    strFields.append(tabS).append(obj.toString()).append(",\n");
                    break;
                default:
                    tab++;
                    strFields.append(tabS).append("{\n");
                    strFields.append(getClassField(obj, obj.getClass(), tab));
                    strFields.append(tabS).append("};\n");
                    tab--;
            }
        }
        return strFields.toString();
    }

    private String getLocalClassFields(Object obj, Field f, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab-1);

        strFields.append(tabS).append(f.getName()).append(":").append(obj.getClass().getName()).append(" = ").append("\n");
        strFields.append(tabS).append("{\n");
        strFields.append(getClassField(obj, obj.getClass(), tab));
        strFields.append(tabS).append("};\n");
        return strFields.toString();
    }

    private StringBuilder initTabS(int tab) {
        StringBuilder tabS = new StringBuilder();
        for (int i = 0; i < tab; i++) {
            tabS.append("    ");
        }
        return tabS;
    }

    private String getArr(Object[] objects, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);

        for (Object obj: objects) {
            strFields.append(getTrueFieldDesc(obj, tab));
        }
        return strFields.toString();
    }

    private String serialArr(Object obj, Field f, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);
        strFields.append(tabS).append(f.getName()).append(":").append(f.getType().getCanonicalName()).append(" = \n");
        strFields.append(getArr((Object[]) obj, tab));
        return strFields.toString();
    }

    private String getColl(List objects, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);
        for (Object obj: objects) {
            strFields.append(getTrueFieldDesc(obj, tab));
        }

        return strFields.toString();
    }

    private String serialColl(Object obj, Field f, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);
        strFields.append(tabS).append(f.getName()).append(":").append(f.getType().getCanonicalName()).append(" = \n");
        strFields.append(getColl((List) obj, tab));
        return strFields.toString();
    }

    private String getMap(Map map, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab-1);
        for (Object obj: map.keySet()) {
            strFields.append(tabS).append("{\n");
            strFields.append(getTrueFieldDesc(obj, tab));
            strFields.append(getTrueFieldDesc(map.get(obj), tab));
            strFields.append(tabS).append("},\n");
        }
        strFields.delete(strFields.length()-2, strFields.length());
        strFields.append(";\n");
        return strFields.toString();
    }

    private String serialMap(Object obj, Field f, int tab) {

        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);
        strFields.append(tabS).append(f.getName()).append(":").append(f.getType().getCanonicalName()).append(" = \n");
        tab++;
        strFields.append(getMap((Map) obj, tab));
        tab--;

        return strFields.toString();
    }


    private StringBuilder getClassField(Object obj, Class cls, int tab) {
        StringBuilder strFields = new StringBuilder();
        StringBuilder tabS = initTabS(tab);
        Field[] fields;

        while (true) {
            try {
                fields = cls.getDeclaredFields();
                for (Field f : fields) {
                    f.setAccessible(true);
                    if (f.get(obj) instanceof String) {
                        strFields.append(tabS).append(f.getName()).append(":").append(f.getType().getCanonicalName()).append(" = ").append(f.get(obj)).append(";\n");
                    } else if (f.getType().isArray()) {
                        strFields.append(serialArr(f.get(obj), f, tab));
                    } else if (f.get(obj) instanceof Collection) {
                        strFields.append(serialColl(f.get(obj), f, tab));
                    } else if (f.get(obj) instanceof Map) {
                        strFields.append(serialMap(f.get(obj), f, tab));
                    } else {
                        switch (f.getType().getTypeName()) {
                            case "int":
                            case "byte":
                            case "short":
                            case "long":
                            case "float":
                            case "double":
                            case "boolean":
                            case "char":
                                strFields.append(tabS).append(f.getName()).append(":").append(f.getType().getCanonicalName()).append(" = ").append(f.get(obj)).append(";\n");
                                break;
                            default:
                                tab++;
                                strFields.append(getLocalClassFields(f.get(obj), f, tab));
                                tab--;
                        }
                    }
                }
                cls = cls.getSuperclass();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                return strFields;
            }
        }

    }

    public String getClassDesc(Object obj) {
        Class<?> cls = obj.getClass();
        classDesc.append(cls.getName()).append(" = \n{\n");
        classDesc.append(getClassField(obj, cls, 1));
        classDesc.append("};");

        return classDesc.toString();
    }
/*
    public String serialize(Object obj) throws Exception {
        if (obj == null)
            return "";
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Map)
            return serializeMap(obj);
        if (obj instanceof Collection)
            return serializeCollection(obj);
        Class<?> clazz = obj.getClass();
        if(clazz.isArray())
            return serializeArray(obj);
        String rslt = ":" + clazz.getTypeName() + "{";
        Field[] flds = clazz.getDeclaredFields();
        for (Field fld : flds) {
            switch (fld.getType().getTypeName()) {
                case "int": // тут примитивные типы обрабатываем
                case "byte":
                case "long":
                case "float":
                    rslt+=fld.getName() + ":" + fld.getType().getTypeName() + "=" + fld.get(obj) + ";";
                    break;
                default: // тут
                    rslt+=fld.getName() + ":" + fld.getType().getTypeName() + "=" + serialize(fld.get(obj)) + ";";
            }
        }
        return rslt;
    }*/
}


